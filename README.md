# README #

### What is this repository for? ###

* *This is a project to demonstrate/learn compile-time template meta-programming concepts in C++.*

* *I am to fill this project with human-readable c++ code as I learn new things*

(Actually I wanted to get acquainted with git-mercurial, and I needed something up and running fast, but here we are)

### How do I get set up? ###

`$> mkdir bin`

`$> g++ main.cpp -o bin/tmp.o`

`$> cd bin`

`$> sudo chmod 777 tmp.o`

`$> ./tmp.o`

or load it up directly into the IDE


### Contribution guidelines ###
eventually...

*  one file per concept

*  comments should only be used to describe the concept

### Who do I talk to? ###

* 0xaddedfacade@gmail.com
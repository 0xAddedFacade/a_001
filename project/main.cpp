/* 
 * File:   main.cpp
 * Author: Bart Michael
 *
 * Created on 25 June 2015, 00:43
 */

#include <cstdlib>
#include <iostream>

//contains absolute value
template <int N>
struct myAbs {
    static const int value = (N < 0) ? -N : N;
};

//containst greatest common denominator
//standart recursion
template<int A, int B>
struct gcd{
    static int const value = (gcd<B, A % B>::value);
};
//ending the recursion
template<int A>
struct gcd<A, 0> {
    static const int value = A;
};

int main(int argc, char** argv) {
    //displaying the compile time evaluation of abs(num)
    int const num = -123456;
    std::cout << "abs("<<num<<"): " << myAbs<num>::value << std::endl;

    //displaying the compile time evaluation of gcd(a,b)
    int const a = 140;
    int const b = 12;
    std::cout << "gcd("<<a<<", "<<b<<"): " << gcd<a,b>::value << std::endl;
}